<?php
/**
* @version		1.1.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2021 Bernhard Sturm / Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.daszeichen.ch
*/
//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}
require_once(dirname(__FILE__).'/../../../includes/extensions/fieldtype.php');
//DOCUMENT_UPLOADS
class PagesAndItemsExtensionFieldtypeDocument_uploads extends PagesAndItemsExtensionFieldtype
{
	function display_config_form($plugin, $type_id, $name, $field_params, $field_id){
		if(!$field_id){
			//new field, set defaults here
			$field_params['showFieldName'] = $this->params->get('showFieldName'); //0
			$field_params['delete_old_document'] = $this->params->get('delete_old_document'); //0
			$field_params['show_src'] = $this->params->get('show_src'); //0
			$field_params['class_name'] = $this->params->get('class_name'); //''
			$field_params['only_source'] = $this->params->get('only_source'); //0
			$field_params['document_dir'] = $this->params->get('document_dir'); //'files/docs/'
		}

		$html = '';

		//New show field name
		$html .= $this->makeShowFieldName($field_id,$field_params);
		//description
		$html .= $this->display_field_description($field_params);
		//validation
		$field_name = JText::_('COM_PAGESANDITEMS_VALIDATION');
		$field_content = '<input type="checkbox" class="checkbox" ';
		if($this->check_if_field_param_is_present($field_params, 'validation')){
			if($field_params['validation']){
				$field_content .= ' checked="checked"';
			}
		}
		$field_content .= 'name="field_params[validation]" value="not_empty" /> '.JText::_('COM_PAGESANDITEMS_NEED_IMAGE');
		$html .= $this->display_field($field_name, $field_content);
		//validation_mesage
		$html .= $this->display_field_validation_message($field_params);

		//old image
		$field_name = JText::_('COM_PAGESANDITEMS_DELETE_OLD_DOCUMENT');
		$field_content = '<input type="checkbox" class="checkbox" ';
		if($this->check_if_field_param_is_present($field_params, 'delete_old_document')){
			$field_content .= ' checked="checked"';
		}
		$field_content .= 'name="field_params[delete_old_document]" value="1" />';
		$html .= $this->display_field($field_name, $field_content);
		//show source
		$field_name = JText::_('COM_PAGESANDITEMS_SHOW_SRC');
		$field_content = '<input type="checkbox" class="checkbox" ';
		if($this->check_if_field_param_is_present($field_params, 'show_src')){
			if($field_params['show_src']){
				$field_content .= ' checked="checked"';
			}
		}
		$field_content .= 'name="field_params[show_src]" value="1" />';
		$html .= $this->display_field($field_name, $field_content);
		//classname
		$field_name = JText::_('COM_PAGESANDITEMS_CLASS_NAME');
		$field_content = '<input type="text" class="width200" value="'.$field_params['class_name'].'" name="field_params[class_name]" /> '.JText::_('COM_PAGESANDITEMS_CLASS_NAME2');
		$html .= $this->display_field($field_name, $field_content);
		//output_only_source
		$field_name = JText::_('COM_PAGESANDITEMS_OUTPUT_ONLY_SOURCE');
		$field_content = '<input type="checkbox" class="checkbox" ';
		if($this->check_if_field_param_is_present($field_params, 'only_source')){
			if($field_params['only_source']){
				$field_content .= ' checked="checked"';
			}
		}
		$field_content .= 'name="field_params[only_source]" value="1" />';
		$html .= $this->display_field($field_name, $field_content);
		//image dir
		$field_name = JText::_('COM_PAGESANDITEMS_DOCUMENT_DIR');
		$field_content = '<input type="text" class="width200" value="'.$field_params['document_dir'].'" name="field_params[document_dir]" /> '.JText::_('COM_PAGESANDITEMS_DOCUMENT_DIR_EXAMPLE');
		$html .= $this->display_field($field_name, $field_content);


		return $html;
	}

	function display_item_edit($field, $field_params, $field_values, $field_value, $new_field, $field_id){

		$html = '';

		//javascript to show hide edit fields for already uploaded images
		$html .= '<script language="javascript"  type="text/javascript">'."\n";
		//function to check extension
		$html .= 'function pi_show_hide_edit_fields(id){'."\n";
		$html .= 'state = document.getElementById(id).style.display;'."\n";
		$html .= 'if(state==\'block\'){'."\n";
		$html .= 'document.getElementById(id).style.display = \'none\';'."\n";
		$html .= '}else{'."\n";
		$html .= 'document.getElementById(id).style.display = \'block\';'."\n";
		$html .= '}'."\n";
		$html .= '}'."\n";
		$html .= '</script>'."\n";

		$document_dir = '';
		if($this->check_if_field_param_is_present($field_params, 'document_dir')){
			$document_dir = $field_params['document_dir'];
		}

		$html .= '<div class="field_type_image fieldtype">';

		//TODO as includes/html/tableitems???
		$images_array = explode('[:-)# ]',$field_value);
		for($n = 0; $n < (count($images_array)-1); $n++){
			$image = $images_array[$n];
			$image_array = explode('-;-',$image);
			$temp_src = $image_array[0];
			$temp_desc = $image_array[1];
			$temp_description = $image_array[2];
			$html .= '<div class="pi_form_wrapper">';
				$html .= '<div class="pi_width20" style="height:2em;display:block;width:100%;">';
					$html .= '<a href="javascript:pi_show_hide_edit_fields(\''.str_replace('.','',$temp_src).'\');">';
						//$html .= '<img src="../'.$document_dir.'/'.$temp_src.'" alt="'.$temp_title.'" style="width: 100px; border: 0;" />';
						$html .= '<a href="'.JURI::root(true).'/'.$document_dir.'/'.$temp_src.'">'.$temp_src.'&nbsp;('.$temp_desc.')</a>';
					$html .= '</a>';
				$html .= '</div>';
				$html .= '<div class="pi_width70">';
					$html .= '<input type="button" class="fltlft" onclick="pi_show_hide_edit_fields(\''.str_replace('.','',$temp_src).'\');" value="'.JText::_('JACTION_EDIT').'" />';
					$html .= '<div>';
						$html .= '<input class="fltlft" style="clear:none;margin-left: 50px;" type="text" name="'.$field_id.'_order[]" value="'.($n+1).'" size="3" />';
						$html .= '<label style="clear:none;min-width: 75px;">';
							$html .= JText::_('COM_PAGESANDITEMS_ORDERING');
						$html .= '</label>';
					$html .= '</div>';
					$html .= '<div>';
						$html .= '<input style="vertical-align:xmiddle;" type="checkbox" name="'.$field_id.'_delete[]" value="'.$n.'" /> ';
						$html .= '<label style="clear:none;min-width: 75px;">';
							$html .= JText::_('JACTION_DELETE');
						$html .= '</label>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';

			$html .= '<div style="display: none;" id="'.str_replace('.','',$temp_src).'">';
			$html .= '<div class="pi_form_wrapper">';
			$html .= '<div class="pi_width20">';
			$html .= '<label class="hasTip" title="'.JText::_('COM_PAGESANDITEMS_DOCUMENT_TIP').'"><span class="editlinktip">'.JText::_('COM_PAGESANDITEMS_DOCUMENT').'</span></label>';
			// $html .= JText::_('COM_PAGESANDITEMS_DOCUMENT');
			$html .= '</span>';
			$html .= '</div>';
			$html .= '<div class="pi_width70">';
			$html .= '<input type="file" name="'.$field_id.'_src[]" onchange="check_extension_'.$field_id.'(this);" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="pi_form_wrapper">';
			$html .= '<div class="pi_width20">';
			$html .= JText::_('COM_PAGESANDITEMS_TITLE_TEXT');
			$html .= ':</div>';
			$html .= '<div class="pi_width70">';
			$html .= '<input type="text" class="width200" value="'.$temp_desc.'" name="'.$field_id.'_title[]" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="pi_form_wrapper">';
			$html .= '<div class="pi_width20">';
			$html .= JText::_('COM_PAGESANDITEMS_DESCRIPTION');
			$html .= ':</div>';
			$html .= '<div class="pi_width70">';
			$html .= '<textarea class="width200 ig_comment" name="'.$field_id.'_description[]">'.$temp_description.'</textarea>';
			$html .= '<input type="hidden" name="'.$field_id.'_old_src[]" value="'.$temp_src.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}

		$html .= '<script language="javascript"  type="text/javascript">'."\n";
		//function to check extension
		$html .= 'function check_extension_'.$field_id.'(thisthing){'."\n";
		$html .= 'value = thisthing.value.toLowerCase();'."\n";
		$html .= 'pos_pdf = value.indexOf(".pdf");'."\n";
		$html .= 'pos_doc = value.indexOf(".doc");'."\n";
		$html .= 'pos_docx = value.indexOf(".docx");'."\n";
		$html .= 'pos_xls = value.indexOf(".xls");'."\n";
			$html .= 'pos_ili = value.indexOf(".ili");'."\n";
    	$html .= 'pos_ods = value.indexOf(".ods");'."\n";
    	$html .= 'pos_ods = value.indexOf(".odt");'."\n";
		$html .= 'pos_xlsx = value.indexOf(".xlsx");'."\n";
		$html .= 'pos_mp3 = value.indexOf(".mp3");'."\n";
		$html .= 'if(pos_mp3==-1 && pos_doc==-1 && pos_pdf==-1 && pos_xls==-1 && pos_docx==-1 && pos_ods==-1 && pos_xlsx==-1 && pos_ili==-1 && pos_odt==-1){'."\n";
		$html .= 'thisthing.value = \'\';'."\n";
		$html .= 'alert(\'wrong file-type. allowed are: pdf, doc, docx, xls, xlsx, odt, ili, mp3 and ods\')'."\n";
		$html .= '}'."\n";
		$html .= '}'."\n";

		//function to add new document
		$html .= 'function new_image_'.$field_id.'(){'."\n";
		$html .= 'pi_image = \''.JText::_('COM_PAGESANDITEMS_DOCUMENT').'\';'."\n";
		$html .= 'pi_image_tooltip = \''.JText::_('COM_PAGESANDITEMS_DOCUMENT_TIP').'\';'."\n";
		$html .= 'extra_code = \'<div class="pi_form_wrapper"><div class="pi_width20">\';'."\n";
		$html .= 'extra_code += \'<label class="hasTip" title="'.JText::_('COM_PAGESANDITEMS_DOCUMENT_TIP').'"><span class="editlinktip">'.JText::_('COM_PAGESANDITEMS_DOCUMENT').'</span></label>\';'."\n";
		$html .= 'extra_code += \'</div><div class="pi_width70">\';'."\n";
		$html .= 'extra_code += \'<input type="file" name="'.$field_id.'_src[]" onchange="check_extension_'.$field_id.'(this);" />\''."\n";
		$html .= 'extra_code += \'</div></div><div class="pi_form_wrapper"><div class="pi_width20">\';'."\n";
		$html .= 'extra_code += \''.JText::_('COM_PAGESANDITEMS_TITLE_TEXT').'\';'."\n";
		$html .= 'extra_code += \':</div><div class="pi_width70">\';'."\n";
		$html .= 'extra_code += \'<input type="text" class="width200" value="" name="'.$field_id.'_title[]" />\';'."\n";
		$html .= 'extra_code += \'</div></div><div class="pi_form_wrapper"><div class="pi_width20">\';'."\n";
		$html .= 'extra_code += \''.strtolower(JText::_('COM_PAGESANDITEMS_DESCRIPTION')).'\';'."\n";
		$html .= 'extra_code += \':</div><div class="pi_width70">\';'."\n";
		$html .= 'extra_code += \'<textarea class="width200 ig_comment" rows="10" cols="10" name="'.$field_id.'_description[]"></textarea>\';'."\n";
		$html .= 'extra_code += \'</div></div>\';'."\n";
		$html .= 'var new_div = document.createElement("div");'."\n";
		$html .= 'new_div.innerHTML = extra_code;'."\n";
		$html .= 'var parent = document.getElementById(\''.$field_id.'_new_images\');'."\n";
		$html .= 'parent.appendChild(new_div);'."\n";
		$html .= '}'."\n";
		$html .= '</script>'."\n";

		$html .= '<div id="'.$field_id.'_new_images">';
		$html .= '</div>';

		$html .= '<div class="new_image_button pi_form_wrapper"><input type="button" value="'.JText::_('COM_PAGESANDITEMS_FIELD_NEW_DOCUMENT').'" onclick="new_image_'.$field_id.'()" /></div>';
		$html .= '</div>';
		$html .= '<input type="hidden" name="'.$field_id.'_document_dir" value="';
		if($this->check_if_field_param_is_present($field_params, 'document_dir')){
			$html .= $field_params['document_dir'];
		}
		$html .=  '" />';
		$html .= '<input type="hidden" name="'.$field_id.'_delete_old_document" value="';
		if($this->check_if_field_param_is_present($field_params, 'delete_old_document')){
			$html .= $field_params['delete_old_document'];
		}
		$html .=  '" />';
		return $html;
	}

	function render_field_output($field, $intro_or_full, $readmore_type=0, $editor_id=0){


		$html = '';

		$html .= '';

			$images_array = explode('[:-)# ]',$field->value);
			$images_array2 = array();
			for($n = 0; $n < (count($images_array)-1); $n++){
				$image = $images_array[$n];
				$image_array = explode('-;-',$image);
				$temp_src = $image_array[0];
				$temp_title = $image_array[1];
				$temp_description = $image_array[2];
				$src_id = str_replace('.','',$temp_src);
				$src_array = explode('.',$temp_src);
				$temp_name = $src_array[0];
				$temp_ext = $src_array[1];
				$document_size=filesize(dirname(__FILE__).'/../../../../../../'.$this->get_field_param($field->params, 'document_dir').'/'.$temp_src);
				$images_array2[] = array($src_id, $temp_src, $temp_description);
				$html .= '<div class="document-item">';
				$html .= '<span class="icon-file-pdf">&nbsp;</span><a href="'.$this->get_field_param($field->params, 'document_dir').$temp_src.'">';
				$html .= addslashes($temp_title).'</a><span class="document-infos"> ('.strtoupper($temp_ext).', '.$this->human_filesize($document_size).'B)</span>';
				if (addslashes($temp_description)):
					$html .= '<p class="document-description">'.addslashes($temp_description).'</p>';
				endif;
				$html .= '';
				$html .= '</div>';
			}

		return addslashes($html);
	}

	function pi_strtolower($string){
		if(function_exists('mb_strtolower')){
			$string = mb_strtolower($string, 'UTF-8');
		}
		return $string;
	}

	function field_save($field, $insert_or_update){

		//field identifier
		$value_name = 'field_values_'.$field->id;

		//get array of values of the file-elements (upload fields)
		$image = $value_name.'_src';

		//get field config things
		$delete_document = JRequest::getVar($value_name.'_delete_old_document', false);
		$document_dir = JRequest::getVar($value_name.'_document_dir', false);

		//get arrays
		$description_array = JRequest::getVar($value_name.'_description', array(), 'post', 'array');
		$title_array = JRequest::getVar($value_name.'_title', array(), 'post', 'array');
		$old_src_array = JRequest::getVar($value_name.'_old_src', array(), 'post', 'array');
		$order_array = JRequest::getVar($value_name.'_order', array(), 'post', 'array');
		$delete_array = JRequest::getVar($value_name.'_delete', array(), 'post', 'array');

		//define allowed extensions
		$allowed_extensions = array('pdf','doc','docx','xls','docx','xlsx','odt','ods','jpg','jpeg','mp3','ili');

		//make name_array and temp_name_array
		$name_array = array();
		$tmp_name_array = array();
		if($_FILES[$image]['name']){
			while(list($key,$value) = each($_FILES[$image]['name'])){
				$name_array[] = $value;
			}
			while(list($key,$value) = each($_FILES[$image]['tmp_name'])){
				$tmp_name_array[] = $value;
			}
		}


		//make array of already uploaded images for reordering
		$images_array = array();
		$total_old_images = 0;
		for($n = 0; $n < count($old_src_array); $n++){
			$temp_name = $name_array[$n];
			$temp_tmp_name = $tmp_name_array[$n];
			$temp_title = $title_array[$n];
			$temp_description = $description_array[$n];
			$temp_order = $order_array[$n];
			if(in_array($n, $delete_array)){
				$temp_delete = 1;
			}else{
				$temp_delete = 0;
			}
			$images_array[] = array($old_src_array[$n], $temp_name, $temp_tmp_name, $temp_title, $temp_description, $temp_order, $temp_delete);
			$total_old_images = $n+1;
		}


		//reorder
		if($total_old_images){
			foreach ($images_array as $key => $row) {
				$order[$key]  = $row[5];
			}
			array_multisort($order, SORT_ASC, $images_array);
		}

		//add the new document
		for($n = $total_old_images; $n < count($title_array); $n++){
			//echo $n;
			$temp_name = $name_array[$n];
			$temp_tmp_name = $tmp_name_array[$n];
			$temp_title = addslashes($title_array[$n]);
			$temp_description = $description_array[$n];
			$temp_order = '';//new images have no order, its just here to prevent undefined index-notices
			$temp_delete = 0;
			$images_array[] = array('', $temp_name, $temp_tmp_name, $temp_title, $temp_description, $temp_order, $temp_delete);
		}

		//start doing the actual processing
		$value_string = '';
		for($n = 0; $n < count($images_array); $n++){

			//get name and extension
			$file = $images_array[$n][1];
			if($file){
				$temp = explode('.',$file);
				$file_name = strtolower($temp[0]);
				$extension = strtolower($temp[1]);
			}else{
				$file_name = '';
				$extension = '';
			}

			//rewrite jpeg to jpg
			if($extension=='jpeg'){
				$extension = 'jpg';
			}


			//make file_name unique
			$file_name = $this->make_filename_unique($file_name, $extension, $document_dir,$delete_document);

			//if not empty
			if($images_array[$n][0] || $images_array[$n][2]){

				//if delete
				if($images_array[$n][6]){
					//delete document
					$this->delete_document($images_array[$n][0], $document_dir);
				}else{
					//do not delete

					//if upload
					if($images_array[$n][1] && in_array($extension, $allowed_extensions)){

						//upload original image
						$prod_img = dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$file_name.'.'.$extension;

						if(move_uploaded_file($images_array[$n][2], $prod_img)){



						}
						else
						{
							// Filename wrong
							die("Falscher Dateiname");
						}
						$image_name = $file_name.'.'.$extension;
					}//end if upload

					//if update without new upload
					if($images_array[$n][0] && !$images_array[$n][1]){
						$image_name = $images_array[$n][0];
					}

					$value_string .= $image_name.'-;-'.$images_array[$n][3].'-;-'.$images_array[$n][4].'[:-)# ]';
				}
			}//end if not empty
		}

		$value = str_replace('"','&quot;',$value_string);
		$value = str_replace("'",'&apos;',$value);


		return $value;
	}



	function make_filename_unique($file_name, $extension, $document_dir,$delete_old){
		if($file_name){
			//rename file if already exist

			if ($delete_old==0):
				if(file_exists(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$file_name.'.'.$extension)){
					$j = 2;
					while (file_exists(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$file_name.'-'.$j.".".$extension)){
						$j = $j + 1;
					}
					$new_name = $file_name . "-" . $j;
				}else{
					$new_name = $file_name;
				}
				else:
				$new_name=$file_name;
			endif;

			//replace spaces by underscores
			$new_name = str_replace(' ', '_', $new_name);
		}else{
			$new_name = '';
		}
		return $new_name;
	}

	function delete_document($temp_old_src, $document_dir){
		$temp_old_src_array = explode('.',$temp_old_src);
		$temp_old_src_name = $temp_old_src_array[0];
		$temp_old_src_extension = $temp_old_src_array[1];
		if(file_exists(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$temp_old_src_name.'.'.$temp_old_src_extension)){
			unlink(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$temp_old_src_name.'.'.$temp_old_src_extension);
		}
		if(file_exists(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$temp_old_src_name.'_thumb.'.$temp_old_src_extension)){
			unlink(dirname(__FILE__).'/../../../../../../'.$document_dir.'/'.$temp_old_src_name.'_thumb.'.$temp_old_src_extension);
		}
	}

	//function to delete images when item is deleted, from PI version 1.4.7
	function item_delete($item_id, $type_id, $field){

		//get document dir
		$document_dir = $this->get_field_param($field->params, 'document_dir');

		$field_id = $field->id;

		//get value of image
		$this->db->setQuery("SELECT value "
		."FROM #__pi_custom_fields_values "
		."WHERE (field_id='$field_id' AND item_id='$item_id') "
		."LIMIT 1 "
		);
		$field_rows = $this->db->loadObjectList();
		foreach($field_rows as $field_row){
			$field_value = $field_row->value;
		}
		$images_array = explode('[:-)# ]',$field_value);

		for($n = 0; $n < count($images_array); $n++){
			$image_stuff = $images_array[$n];
			$image_stuff_array = explode('-;-',$image_stuff);
			$image = $image_stuff_array[0];
			if($image){
				$this->delete_document($image, $document_dir);
			}
		}
	}

	//function to delete documents when field or itemtype is deleted, from PI version 1.4.7
	function field_delete($field){

		//get image dir
		$document_dir = $this->get_field_param($field->params, 'document_dir');

		$field_id = $field->id;

		//get values of images
		$this->db->setQuery("SELECT value "
		."FROM #__pi_custom_fields_values "
		."WHERE field_id='$field_id' "
		);
		$field_rows = $this->db->loadObjectList();
		foreach($field_rows as $field_row){
			$field_value = $field_row->value;
			$images_array = explode('[:-)# ]',$field_value);
			for($n = 0; $n < count($images_array); $n++){
				$image_stuff = $images_array[$n];
				$image_stuff_array = explode('-;-',$image_stuff);
				$image = $image_stuff_array[0];
				if($image){
					$this->delete_document($image, $document_dir);
				}
			}


		}
	}

	function onFieldtypeFrontend(&$article,$field, $item_id,$type_id)
	{

		if($field->plugin != 'image_gallery')
		{
			return true;
		}

		// Don't repeat the for each instance of this fieldtype in a page!
		static $included_image_gallery;
		if (!$included_image_gallery)
		{
			//FB::dump($field);
			//JHTML::_('behavior.modal');
			//$document =& JFactory::getDocument();
			//$document->addStyleSheet('administrator/components/com_pagesanditems/extensions/fieldtypes/image_gallery/image_gallery.css');
			$included_image_gallery = 1;
		}

		return true;
	}

	/* Calculate the filesize */
	function human_filesize($bytes, $decimals = 1) {
	  $sz = 'BKMGTP';
	  $factor = floor((strlen($bytes) - 1) / 3);
	  if ($bytes>=0):
	  	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) .'&nbsp;'. @$sz[$factor];
	  else:
	  	return "FILE DELETED!";
	  endif;
	}


}

?>
